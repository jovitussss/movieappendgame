class FilmDetail {
  final bool? adult;
  final dynamic backdropPath;
  final dynamic belongsToCollection;
  final int? budget;
  final List<dynamic>? genres;
  final String? homepage;
  final int? id;
  final dynamic imdbId;
  final String? originalLanguage;
  final String? originalTitle;
  final String? overview;
  final double? popularity;
  final String? posterPath;
  final List<dynamic>? productionCompanies;
  final List<ProductionCountries>? productionCountries;
  final String? releaseDate;
  final int? revenue;
  final int? runtime;
  final List<dynamic>? spokenLanguages;
  final String? status;
  final String? tagline;
  final String? title;
  final bool? video;
  final num? voteAverage;
  final int? voteCount;

  FilmDetail({
    this.adult,
    this.backdropPath,
    this.belongsToCollection,
    this.budget,
    this.genres,
    this.homepage,
    this.id,
    this.imdbId,
    this.originalLanguage,
    this.originalTitle,
    this.overview,
    this.popularity,
    this.posterPath,
    this.productionCompanies,
    this.productionCountries,
    this.releaseDate,
    this.revenue,
    this.runtime,
    this.spokenLanguages,
    this.status,
    this.tagline,
    this.title,
    this.video,
    this.voteAverage,
    this.voteCount,
  });

  FilmDetail.fromJson(Map<String, dynamic> json)
      : adult = json['adult'] as bool?,
        backdropPath = json['backdrop_path'],
        belongsToCollection = json['belongs_to_collection'],
        budget = json['budget'] as int?,
        genres = json['genres'] as List?,
        homepage = json['homepage'] as String?,
        id = json['id'] as int?,
        imdbId = json['imdb_id'],
        originalLanguage = json['original_language'] as String?,
        originalTitle = json['original_title'] as String?,
        overview = json['overview'] as String?,
        popularity = json['popularity'] as double?,
        posterPath = json['poster_path'] as String?,
        productionCompanies = json['production_companies'] as List?,
        productionCountries = (json['production_countries'] as List?)?.map((dynamic e) => ProductionCountries.fromJson(e as Map<String,dynamic>)).toList(),
        releaseDate = json['release_date'] as String?,
        revenue = json['revenue'] as int?,
        runtime = json['runtime'] as int?,
        spokenLanguages = json['spoken_languages'] as List?,
        status = json['status'] as String?,
        tagline = json['tagline'] as String?,
        title = json['title'] as String?,
        video = json['video'] as bool?,
        voteAverage = json['vote_average'] as num?,
        voteCount = json['vote_count'] as int?;

  Map<String, dynamic> toJson() => {
    'adult' : adult,
    'backdrop_path' : backdropPath,
    'belongs_to_collection' : belongsToCollection,
    'budget' : budget,
    'genres' : genres,
    'homepage' : homepage,
    'id' : id,
    'imdb_id' : imdbId,
    'original_language' : originalLanguage,
    'original_title' : originalTitle,
    'overview' : overview,
    'popularity' : popularity,
    'poster_path' : posterPath,
    'production_companies' : productionCompanies,
    'production_countries' : productionCountries?.map((e) => e.toJson()).toList(),
    'release_date' : releaseDate,
    'revenue' : revenue,
    'runtime' : runtime,
    'spoken_languages' : spokenLanguages,
    'status' : status,
    'tagline' : tagline,
    'title' : title,
    'video' : video,
    'vote_average' : voteAverage,
    'vote_count' : voteCount
  };
}

class ProductionCountries {
  final String? iso31661;
  final String? name;

  ProductionCountries({
    this.iso31661,
    this.name,
  });

  ProductionCountries.fromJson(Map<String, dynamic> json)
      : iso31661 = json['iso_3166_1'] as String?,
        name = json['name'] as String?;

  Map<String, dynamic> toJson() => {
    'iso_3166_1' : iso31661,
    'name' : name
  };
}