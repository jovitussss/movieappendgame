//save all information of API
class APIInfo{
  final String APIKEYv3 = '032465a3e27cdf91e6f1e5df6f3622b7';
  final String baseURLposterPath = 'https://image.tmdb.org/t/p/w500/';

  String singleFilmURLgenerate(int film_id){
    return 'https://api.themoviedb.org/3/movie/${film_id}?api_key=032465a3e27cdf91e6f1e5df6f3622b7&language=en-US';
  }

  String listNewFilmDiscover(int page){
    return "https://api.themoviedb.org/3/discover/movie?api_key=032465a3e27cdf91e6f1e5df6f3622b7"
        "&language=en-US"
        "&sort_by=release_date.desc"
        "&include_adult=false"
        "&include_video=true"
        "&page=$page"
        "&with_watch_monetization_types=flatrate";
  }

  String ListPopularyFilmDiscover(int page){
    return 'https://api.themoviedb.org/3/discover/movie?api_key=032465a3e27cdf91e6f1e5df6f3622b7'
        '&language=en-US'
        '&sort_by=popularity.desc'
        '&include_adult=true'
        '&include_video=true'
        '&page=$page'
        '&with_watch_monetization_types=flatrate';
  }

  String ListBestFilmOnVote(int page){
    return 'https://api.themoviedb.org/3/discover/movie?api_key=032465a3e27cdf91e6f1e5df6f3622b7'
        '&language=en-US'
        '&sort_by=vote_count.desc'
        '&include_adult=true'
        '&include_video=true'
        '&page=$page'
        '&with_watch_monetization_types=flatrate';
  }

  String ListSearchFilm(String querry){
    return 'https://api.themoviedb.org/3/search/movie?api_key=032465a3e27cdf91e6f1e5df6f3622b7&language=en-US&query=$querry&page=1&include_adult=true';
  }

  String GetSimilarMovie(int movieID){
    return 'https://api.themoviedb.org/3/movie/${movieID}/similar?api_key=032465a3e27cdf91e6f1e5df6f3622b7&language=en-US&page=1';
  }
}