import 'dart:convert';

import 'package:movieappendgame/data/api/api_moviedb_information.dart';
import 'package:rxdart/rxdart.dart';

import '../models/film_detail.dart';
import 'package:http/http.dart' as http;

class MovieDataSource {
  Future<FilmDetail> fetchSingleFilm(
    String URLPATH,
      ) async{
    final response = await http.get(Uri.parse(URLPATH));
    if(response.statusCode == 200){
      final movieInfo = json.decode(response.body);
      return FilmDetail.fromJson(movieInfo);
    }
    else return FilmDetail();
  }


  Future<List<FilmDetail>?> fetchListFilm(
    String URLPath,
      BehaviorSubject<List<FilmDetail>> temp,
  ) async {
    final response = await http.get(Uri.parse(URLPath));
    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      final result = data["results"] as List;
      final List<FilmDetail> resultFinal = [];
      result.forEach(
        (element) {
          resultFinal.add(FilmDetail.fromJson(element));
        },
      );
      temp.add(resultFinal);
      return resultFinal;
    } else {
      return [];
    }
  }

  BehaviorSubject<List<FilmDetail>> getNewFilmDiscover() {
    late BehaviorSubject<List<FilmDetail>> newFilmStream;
    newFilmStream = BehaviorSubject.seeded([]);
    fetchListFilm(APIInfo().listNewFilmDiscover(1), newFilmStream);
    return newFilmStream;
  }

  BehaviorSubject<List<FilmDetail>> getPopularFilmDiscover() {
    late BehaviorSubject<List<FilmDetail>> newFilmStream;
    newFilmStream = BehaviorSubject.seeded([]);
    fetchListFilm(APIInfo().ListPopularyFilmDiscover(1), newFilmStream);
    return newFilmStream;
  }

  BehaviorSubject<List<FilmDetail>> getBestVotedFilmDiscover() {
    late BehaviorSubject<List<FilmDetail>> newFilmStream;
    newFilmStream = BehaviorSubject.seeded([]);
    fetchListFilm(APIInfo().ListBestFilmOnVote(1), newFilmStream);
    return newFilmStream;
  }

  BehaviorSubject<List<FilmDetail>> getNewFilm(int page){
    late BehaviorSubject<List<FilmDetail>> newFilmStream;
    newFilmStream = BehaviorSubject.seeded([]);
    fetchListFilm(APIInfo().ListPopularyFilmDiscover(page), newFilmStream);
    return newFilmStream;
  }

  BehaviorSubject<List<FilmDetail>> getSimilarMovie(int movieID){
    late BehaviorSubject<List<FilmDetail>> newFilmStream;
    newFilmStream = BehaviorSubject.seeded([]);
    fetchListFilm(APIInfo().GetSimilarMovie(movieID), newFilmStream);
    return newFilmStream;
  }

  BehaviorSubject<List<FilmDetail>> getSearchList(String querry) {
    late BehaviorSubject<List<FilmDetail>> newFilmStream;
    newFilmStream = BehaviorSubject.seeded([]);
    fetchListFilm(APIInfo().ListSearchFilm(querry), newFilmStream);
    return newFilmStream;
  }

 
}
