import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/screen/detail_screen.dart';
import 'package:movieappendgame/presentations/screen/favorite_screen.dart';
import 'package:movieappendgame/presentations/screen/home_screen.dart';
import 'package:movieappendgame/presentations/screen/search_screen.dart';
import 'package:movieappendgame/presentations/screen/user_screen.dart';
import 'package:movieappendgame/presentations/widget/general_use/app_bar.dart';
import 'package:movieappendgame/presentations/widget/general_use/more_film.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp>{
  int selectedIndex = 0;

  List<Widget> _screenWidget = <Widget>[
    HomeScreen(),
    SearchScreen(),
    FavoriteScreen(),
    // UserScreen(),
    UserScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          appBar: CustomAppBar().customAppBar(),
          bottomNavigationBar: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home, color: Colors.black,), label: "home"),
              BottomNavigationBarItem(icon: Icon(Icons.search, color: Colors.black,), label: "search"),
              BottomNavigationBarItem(icon: Icon(Icons.favorite, color: Colors.black,), label: "favorite"),
              BottomNavigationBarItem(icon: Icon(Icons.person, color: Colors.black,), label: "profile"),
            ],
            onTap: (index){
              setState(() {
                selectedIndex = index;
              });
            },
          ),
          body: _screenWidget.elementAt(selectedIndex),
        ),
      ),
    );
  }

}