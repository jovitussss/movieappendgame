import 'package:movieappendgame/data/data_source/movie_data_source.dart';
import 'package:movieappendgame/data/models/film_detail.dart';
import 'package:rxdart/rxdart.dart';

class FilmStreamReturn{
  final BehaviorSubject<List<FilmDetail>> listNewFilmStream = MovieDataSource().getNewFilmDiscover();
  final BehaviorSubject<List<FilmDetail>> listPopularFilmStream = MovieDataSource().getPopularFilmDiscover();
  final BehaviorSubject<List<FilmDetail>> listBestVotedFilmStream = MovieDataSource().getBestVotedFilmDiscover();
  Stream<List<FilmDetail>> listNewFilmStreamReturn(){
    return listNewFilmStream.stream;
  }

  Stream<List<FilmDetail>> listPopularFilmStreamReturn(){
    return listPopularFilmStream.stream;
  }

  Stream<List<FilmDetail>> listBestVotedFilmStreamReturn(){
    return listBestVotedFilmStream.stream;
  }

  Stream<List<FilmDetail>> listSimilarFilmStreamReturn(int movieID){
    return MovieDataSource().getSimilarMovie(movieID).stream;
  }
}