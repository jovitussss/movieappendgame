import 'package:movieappendgame/domain/logic/load_more.dart';
import 'package:rxdart/rxdart.dart';

import '../../data/api/api_moviedb_information.dart';
import '../../data/data_source/movie_data_source.dart';
import '../../data/models/film_detail.dart';

class PopularityFilmLoadMore extends LoadMore{
  late BehaviorSubject<List<FilmDetail>> filmLoadMore;
  int page = 1;

  PopularityFilmLoadMore() {
    filmLoadMore = BehaviorSubject.seeded([]);
    MovieDataSource().fetchListFilm(APIInfo().ListPopularyFilmDiscover(1), filmLoadMore);
  }


  @override
  void makeLoadMore() {
    page=page+1;
    MovieDataSource().fetchListFilm(APIInfo().ListPopularyFilmDiscover(page), filmLoadMore);
  }

  Stream<List<FilmDetail>> returnSream(){
    return filmLoadMore.stream;
  }
}