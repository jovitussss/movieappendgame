import 'package:movieappendgame/data/data_source/movie_data_source.dart';
import 'package:movieappendgame/data/models/film_detail.dart';

class Convert{
  Convert(Stream<List<FilmDetail>> stream){
    this._stream = stream;
  }
  Stream<List<FilmDetail>> _stream = MovieDataSource().getNewFilm(1).stream;
  List<FilmDetail> convert(){
    List<FilmDetail> temp = [];
    _stream.listen((event) {
      for(FilmDetail _filmDetail in event){
        print(_filmDetail.title);
        temp.add(_filmDetail);
      }
    });
    return temp;
  }

}