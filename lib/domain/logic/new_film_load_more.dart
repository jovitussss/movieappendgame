import 'package:movieappendgame/data/api/api_moviedb_information.dart';
import 'package:movieappendgame/data/data_source/movie_data_source.dart';
import 'package:movieappendgame/data/models/film_detail.dart';
import 'package:movieappendgame/domain/logic/convert_stream_to_list.dart';
import 'package:movieappendgame/domain/logic/load_more.dart';
import 'package:rxdart/rxdart.dart';

class NewFilmLoadMore extends LoadMore {
  late BehaviorSubject<List<FilmDetail>> filmLoadMore;
  int page = 1;

  NewFilmLoadMore() {
    filmLoadMore = BehaviorSubject.seeded([]);
    MovieDataSource().fetchListFilm(APIInfo().listNewFilmDiscover(1), filmLoadMore);
  }


  @override
  void makeLoadMore() {
    page=page+1;
    MovieDataSource().fetchListFilm(APIInfo().listNewFilmDiscover(page), filmLoadMore);
  }

  Stream<List<FilmDetail>> returnSream(){
    return filmLoadMore.stream;
  }

}
