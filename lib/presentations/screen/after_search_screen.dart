import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/widget/general_use/app_bar.dart';

import '../widget/after_search_screen/after_search_screen_body.dart';

class AfterSearchScreen extends StatelessWidget{
  late final String querrySearch;
  AfterSearchScreen(String querry){
    this.querrySearch = querry;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar().customAppBar(),
      body: AfterSearchScreenBody(querrySearch),
    );
  }

}