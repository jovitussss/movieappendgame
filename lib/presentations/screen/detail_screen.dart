import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/widget/general_use/app_bar.dart';

import '../widget/detail_screen/detail_screen_body.dart';

class DetailScreen extends StatelessWidget{
  int id = 0;
  DetailScreen(int filmid){
    this.id = filmid;
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: CustomAppBar().customAppBar(),
            body: DetailScreenBody(id)));
  }

}