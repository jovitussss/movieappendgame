import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/widget/general_use/app_bar.dart';
import 'package:movieappendgame/presentations/widget/general_use/more_film.dart';

class LoadMoreScreen extends StatelessWidget{
  int useScreen = 0;
  LoadMoreScreen(int useScreenTemp){
    this.useScreen = useScreenTemp;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar().customAppBar(),
      body: MoreFilm(useScreen),
    );
  }

}