

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/widget/general_use/app_bar.dart';
import 'package:movieappendgame/presentations/widget/user_screen/user_screen_body.dart';

import '../widget/home_screen/home_screen_body.dart';

class UserScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return UserScreenBody();
  }}