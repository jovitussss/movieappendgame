import 'package:flutter/material.dart';

class UserScreenBody extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();

}
class _State extends State<UserScreenBody>{
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 35),
          child: Center(
            child: CircleAvatar(
              radius: MediaQuery.of(context).size.width / 4,
              backgroundImage: NetworkImage('https://chiase24.com/wp-content/uploads/2022/02/tang-hap-hanh-anh-avatar-hai-haeac-nhan-la-ba_t-caea_i-1.jpg'),
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 35),
          child: Center(
            child: Text('Abc xyz',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: MediaQuery.of(context).size.height / 35,
            ),),
          ),
        ),
        Spacer(),
        Padding(
            padding: EdgeInsets.all(1),
          child: TextButton(
            onPressed: () {  },
            child: Text('Đăng Xuất',
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.height / 35,
            ),),

          ),
        ),
      ],
    );
  }

}