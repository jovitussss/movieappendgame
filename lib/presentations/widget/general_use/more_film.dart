import 'dart:async';

import 'package:flutter/material.dart';
import 'package:movieappendgame/data/api/api_moviedb_information.dart';
import 'package:movieappendgame/data/data_source/movie_data_source.dart';
import 'package:movieappendgame/data/models/film_detail.dart';
import 'package:movieappendgame/domain/logic/best_film_on_vote_loadmore.dart';
import 'package:movieappendgame/domain/logic/load_more.dart';
import 'package:movieappendgame/domain/logic/new_film_load_more.dart';
import 'package:movieappendgame/domain/logic/populary_film_loadmore.dart';
import 'package:movieappendgame/domain/use_case/newFilmStream.dart';
import 'package:movieappendgame/presentations/widget/home_screen/home_screen_item_hozizontal.dart';

import '../../screen/detail_screen.dart';

class MoreFilm extends StatefulWidget{
  late int useScreen;
  MoreFilm(int useScreenTemp){
    this.useScreen = useScreenTemp;
  }
  @override
  State<StatefulWidget> createState() => _MoreFilmState(useScreen);

}
class _MoreFilmState extends State<MoreFilm>{
  late int useScreen;
  late LoadMore newFilmLoadMore;
  late ScrollController _controller;
  late List<FilmDetail> _listFilm;

  _MoreFilmState(int useScreen){
    this.useScreen = useScreen;
  }
  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    if (useScreen ==0){
      newFilmLoadMore = NewFilmLoadMore();
    } else if(useScreen == 1){
      newFilmLoadMore = PopularityFilmLoadMore();
    } else {
      newFilmLoadMore = BestFilmOnVoteLoadMore();
    }
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return StreamBuilder(
        stream: newFilmLoadMore.returnSream(),
        builder: (context,AsyncSnapshot<List<FilmDetail>> snapshot){
          if (!snapshot.hasData){
            return CircularProgressIndicator();
          }
          else
            {
          List<FilmDetail> data = snapshot.data ?? [];
          return GridView.builder(
            controller: _controller,
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisExtent: 310,
                    mainAxisSpacing: 20),
                itemCount: data.length,
                itemBuilder: (BuildContext ctx, index) {
                  return Container(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailScreen(data[index].id  ?? 190293)),
                      ),
                      child: HomeScreenItemHozizontalList(size: size,
                      image: data[index].posterPath != null ?'https://image.tmdb.org/t/p/w500/${data[index].posterPath}' : "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
                          film_name: data[index].title ?? "cant load",),
                    ),
                  );
                });}
        });
  }


  void _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        newFilmLoadMore.makeLoadMore();
      });
    }
  }
}

