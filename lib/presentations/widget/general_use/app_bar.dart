import 'package:flutter/material.dart';

class CustomAppBar{
  AppBar customAppBar(){
    return AppBar(
      backgroundColor: Colors.lightGreen,
      title: Center(
        child: Text(
          'MOVIE INFO'
        ),
      ),
    );
  }
}