import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:movieappendgame/data/api/api_moviedb_information.dart';
import 'package:movieappendgame/data/data_source/movie_data_source.dart';
import 'package:movieappendgame/data/models/film_detail.dart';
import 'package:movieappendgame/domain/use_case/newFilmStream.dart';

import '../../screen/detail_screen.dart';
import '../home_screen/home_screen_item_hozizontal.dart';

class DetailScreenBody extends StatefulWidget{
  late int filmID;

  DetailScreenBody(int filmID){
    this.filmID = filmID;
  }
  @override
  State<StatefulWidget> createState() => _DetailScreenBodyState(filmID);
}

class _DetailScreenBodyState extends State<DetailScreenBody>{
  late int filmID;
  _DetailScreenBodyState(int filmID){
    this.filmID = filmID;
  }


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Material(
      child: FutureBuilder(
          future:MovieDataSource().fetchSingleFilm(APIInfo().singleFilmURLgenerate(filmID)),
          builder: (context, AsyncSnapshot<FilmDetail> snapshot){
            if(!snapshot.hasData){
              return Center(child: CircularProgressIndicator());
            }
            else {
              final data = snapshot.data;
              return SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: ListView(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        children: <Widget>[
                          Container(
                            child: Image.network(data?.posterPath != null ? 'https://image.tmdb.org/t/p/w500/${data?.posterPath}' : "https://img.freepik.com/free-vector/online-cinema-banner-with-open-clapper-board-film-strip_1419-2242.jpg?w=2000",
                              height: size.height * 0.3,
                              width: size.width,),
                          ),


                          Container(
                            child: Center(
                              child: Text(
                                data?.title ?? "cant read",
                                style: TextStyle(
                                  height: 2,
                                  fontSize: 25,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(100, 10, 100, 0),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Text(
                              data?.voteAverage.toString() ?? "0",
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            child: Text(
                              data?.releaseDate ?? '0',
                            ),
                          ),
                          Spacer(),
                          Container(
                            child: IconButton(
                              icon: Icon(Icons.favorite_border),
                              onPressed: (){
                                
                              },
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                      child: Container(
                        child: Text(
                          data?.overview ?? "no overview",
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),

                  Padding(
                      padding: EdgeInsets.all(16),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(16),
                            child: Text('Phim Tương Tự',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),)),
                        StreamBuilder<List<FilmDetail>>(
                          stream: FilmStreamReturn().listSimilarFilmStreamReturn(filmID),
                            builder: (context, snapshot){
                            if(!snapshot.hasData){
                              return CircularProgressIndicator();
                            }
                            else{
                              final data = snapshot.data ?? [];
                              return Container(
                                height: 350,
                                child: ListView.builder(
                                  // shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) {

                                      return Container(
                                        child: GestureDetector(
                                          onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => DetailScreen(data[index].id  ?? 190293)),
                                          ),
                                          child: HomeScreenItemHozizontalList(
                                              size: size,
                                              image:
                                              data[index].posterPath != null ?'https://image.tmdb.org/t/p/w500/${data[index].posterPath}' : "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
                                              film_name: data[index].title ?? ""),
                                        ),
                                      );
                                    },
                                    itemCount: data.length),
                              );
                            }
                            }
                        ),
                      ],
                    ),
                  ),
                  ],
                ),
              );
            }
          }),
    );
  }

}