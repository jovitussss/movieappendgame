import 'package:flutter/material.dart';

import '../home_screen/home_screen_item_hozizontal.dart';


class FavoriteBody extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();

}

class _State extends State<FavoriteBody>{
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GridView.count(
      primary: false,
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      childAspectRatio: 0.794,
      children: <Widget>[
        Container(
          child: HomeScreenItemHozizontalList(size: size,
              image: "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
              film_name: 'movie name'),
        ),
        Container(
          child: HomeScreenItemHozizontalList(size: size,
              image: "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
              film_name: 'movie name'),
        ),
        Container(
          child: HomeScreenItemHozizontalList(size: size,
              image: "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
              film_name: 'movie name'),
        ),
        Container(
          child: HomeScreenItemHozizontalList(size: size,
              image: "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
              film_name: 'movie name'),
        ),
        Container(
          child: HomeScreenItemHozizontalList(size: size,
              image: "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
              film_name: 'movie name'),
        ),
        Container(
          child: HomeScreenItemHozizontalList(size: size,
              image: "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
              film_name: 'movie name'),
        ),

      ],
    );
  }

}