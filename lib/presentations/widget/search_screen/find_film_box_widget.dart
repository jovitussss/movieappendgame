
import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/screen/after_search_screen.dart';

class FindFilmBox extends StatelessWidget {
  const FindFilmBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textController = TextEditingController();
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 5, 15, 0),
      child: SizedBox(
        height: 50,
        child: TextField(
          controller: textController,
          onSubmitted:(String) => Navigator.push(
              context,
          MaterialPageRoute(builder: (context) => AfterSearchScreen(textController.text))),
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            hintText: 'Tìm phim bạn muốn xem',
            hintStyle: TextStyle(
              fontSize: 13,
            ),
          ),
        ),
      ),
    );
  }
}