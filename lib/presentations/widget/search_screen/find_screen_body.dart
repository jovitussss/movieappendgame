import 'package:flutter/material.dart';

import 'find_film_box_widget.dart';

class FindingBody extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => FindingBodyState();
}

class FindingBodyState extends State<FindingBody>{
  @override
  Widget build(BuildContext context) {
    return FindFilmBox();
  }

}
