
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreenItemHozizontalList extends StatelessWidget {
  const HomeScreenItemHozizontalList({
    Key? key,
    required this.size,
    required this.image,
    required this.film_name,
  }) : super(key: key);

  final Size size;
  final String image, film_name;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: Container(

        width: size.width * 0.4,
        margin: EdgeInsets.all(2),
        child: Column(
          children: <Widget>[
            Image.network(image,
              fit: BoxFit.cover,
            height: 200,
            ),
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),

                color: Colors.white,
                // boxShadow: [
                //   BoxShadow(offset: Offset(0,10),
                //     blurRadius: 50,
                //   )
                // ],

              ),
              child: Center(
                child: Text(
                  '$film_name',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
