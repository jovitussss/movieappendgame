
import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  const HeaderText({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5),
      child: Text("$text",
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),),
    );
  }
}