import 'package:flutter/material.dart';
import 'package:movieappendgame/presentations/screen/load_more_screen.dart';

class SeeMoreButton extends StatelessWidget {
  late final whereButton;
  SeeMoreButton(int whereButton){
    this.whereButton = whereButton;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 5),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        color: Colors.brown,
        onPressed: () {
            Navigator.push(context,
            MaterialPageRoute(builder: (context) => LoadMoreScreen(whereButton))
            );
        },
        child: Center(
          child: Text("Xem Thêm",
            style: TextStyle(
              color: Colors.white,
            ),),
        ),),
    );
  }
}