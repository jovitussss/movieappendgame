import 'package:flutter/material.dart';
import 'package:movieappendgame/domain/use_case/newFilmStream.dart';
import 'package:movieappendgame/presentations/screen/detail_screen.dart';
import 'package:movieappendgame/presentations/widget/home_screen/see_more_button.dart';
import 'package:rxdart/rxdart.dart';
import '../../../data/models/film_detail.dart';
import 'header_with_custom_text.dart';
import 'home_screen_item_hozizontal.dart';

class HomeScreenBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HCBState();
}

class HCBState extends State<HomeScreenBody> {
  late BehaviorSubject<List<FilmDetail>> newFilmList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      children: <Widget>[
                        HeaderText(
                          text: 'Phim Mới',
                        ),
                        Spacer(),
                        SeeMoreButton(0),
                      ],
                    ),
                  ),
                  StreamBuilder<List<FilmDetail>>(
                      stream: FilmStreamReturn().listNewFilmStreamReturn(),
                      builder: (context, snapshot) {
                        if(snapshot.connectionState == ConnectionState.active){
                          if(!snapshot.hasData) return CircularProgressIndicator();
                          else{
                          final data = snapshot.data ?? [];
                          return Container(
                            height: 350,
                            child: ListView.builder(
                                // shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return Container(
                                    child: GestureDetector(
                                      onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => DetailScreen(
                                                data[index].id ?? 190293)),
                                      ),
                                      child: HomeScreenItemHozizontalList(
                                          size: size,
                                          image: data[index].posterPath != null
                                              ? 'https://image.tmdb.org/t/p/w500/${data[index].posterPath}'
                                              : "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
                                          film_name: data[index].title ?? ""),
                                    ),
                                  );
                                },
                                itemCount: data.length),
                          );}
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                      ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      children: <Widget>[
                        HeaderText(
                          text: 'Phim Phổ Biến',
                        ),
                        Spacer(),
                        SeeMoreButton(1),
                      ],
                    ),
                  ),
                  StreamBuilder<List<FilmDetail>>(
                      stream: FilmStreamReturn().listPopularFilmStreamReturn(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return CircularProgressIndicator();
                        } else {
                          final data = snapshot.data ?? [];
                          return Container(
                            height: 350,
                            child: ListView.builder(
                                // shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                      child: GestureDetector(
                                    onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailScreen(
                                              data[index].id ?? 190293)),
                                    ),
                                    child: HomeScreenItemHozizontalList(
                                        size: size,
                                        image: data[index].posterPath != null
                                            ? 'https://image.tmdb.org/t/p/w500/${data[index].posterPath}'
                                            : "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
                                        film_name: data[index].title ?? ""),
                                  ));
                                },
                                itemCount: data.length),
                          );
                        }
                      }),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      children: <Widget>[
                        HeaderText(
                          text: 'Phim Được Đánh Giá Cao',
                        ),
                        Spacer(),
                        SeeMoreButton(2),
                      ],
                    ),
                  ),
                  StreamBuilder<List<FilmDetail>>(
                      stream:
                          FilmStreamReturn().listBestVotedFilmStreamReturn(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return CircularProgressIndicator();
                        } else {
                          final data = snapshot.data ?? [];
                          return Container(
                            height: 350,
                            child: ListView.builder(
                                // shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                      child: GestureDetector(
                                    onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailScreen(
                                              data[index].id ?? 190293)),
                                    ),
                                    child: HomeScreenItemHozizontalList(
                                        size: size,
                                        image: data[index].posterPath != null
                                            ? 'https://image.tmdb.org/t/p/w500/${data[index].posterPath}'
                                            : "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
                                        film_name: data[index].title ?? ""),
                                  ));
                                },
                                itemCount: data.length),
                          );
                        }
                      }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
