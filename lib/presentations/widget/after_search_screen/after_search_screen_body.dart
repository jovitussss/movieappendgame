import 'package:flutter/material.dart';
import 'package:movieappendgame/data/data_source/movie_data_source.dart';
import 'package:movieappendgame/data/models/film_detail.dart';

import '../../screen/detail_screen.dart';
import '../home_screen/home_screen_item_hozizontal.dart';

class AfterSearchScreenBody extends StatefulWidget{
  late final String searchQuerry;
  AfterSearchScreenBody(String querry){
    this.searchQuerry = querry;
  }
  @override
  State<StatefulWidget> createState() => ASState(searchQuerry);

}

class ASState extends State<AfterSearchScreenBody>{
  late final String searchQuerry;
  ASState(String querry){
    this.searchQuerry = querry;
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return StreamBuilder<List<FilmDetail>>(
      stream: MovieDataSource().getSearchList(searchQuerry),
      builder: (context, snapshot){
        if(!snapshot.hasData){
          return CircularProgressIndicator();
        }
        else {
          final data = snapshot.data ?? List<FilmDetail>.empty();
          if (data.isEmpty == true){
            return Center(
              child: Text(
                'Không thể tìm thấy phim này'
              ),
            );
          }
          else{
            return GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisExtent: 310,
                    mainAxisSpacing: 20),
                itemCount: data.length,
                itemBuilder: (BuildContext ctx, index) {
                  return Container(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailScreen(data[index].id  ?? 190293)),
                      ),
                      child: HomeScreenItemHozizontalList(size: size,
                        image: data[index].posterPath != null ?'https://image.tmdb.org/t/p/w500/${data[index].posterPath}' : "https://cdn5.vectorstock.com/i/1000x1000/38/39/cinema-banner-movie-watching-vector-19833839.jpg",
                        film_name: data[index].title ?? "cant load",),
                    ),
                  );
                });
          }
        }
      },
    );
  }

}